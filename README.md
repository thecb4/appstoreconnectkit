# AppStoreConnectKit

AppStoreConnectKit is a programatic way to intereact with AppStoreConnect

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [XMLCoder](https://github.com/MaxDesiatov/XMLCoder) - Coder framework for Encoding and Decoding
* [Shelltr](https://gitlab.com/thecb4/Shelltr) - Wrapper for shell commands
* [Swift-sh](https://github.com/mxcl/swift-sh) - Swift scripting made easy by Max Howell


### Prerequisites

* Swift 4.2


### Installing

Package Manager

```
.package(url: "https://gitlab.com/thecb4/AppStoreConnectKit.git", .exact("0.1.0"))
```

## Using

Very straight forward to use, like any other codable

```swift
import AppStoreConnectKit

let inputPath = FileManager.default.currentDirectoryPath + "/metadata.xml"
let inputURL = URL(fileURLWithPath: inputPath)
let input = try String(contentsOf: inputURL)
let decoder = XMLDecoder()
let metadata = try decoder.decode(AppMetaData.self, from: input.data(using: .utf8)!)
print(metadata.software.metadata.gameCenter)
```

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
swift test
```

## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 



### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library. 


## Authors

* **'Cavelle Benjamin'** - *Initial work* - [thecb4](https://thecb4.io)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
