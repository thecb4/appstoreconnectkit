#!/usr/bin/swift sh

import Foundation
import Shelltr // https://gitlab.com/thecb4/Shelltr == 0.1.10

if #available(macOS 10.13, *) {
  Shell.work {
    try Shell.assertModified(file: "CHANGELOG.md")
    // try Shell.assertModified(file: "COMMIT_MESSAGE.md")
    try Shell.rm(content: ".build", recursive: true, force: true)
    try Shell.rm(content: "DerivedData", recursive: true, force: true)
    try Shell.swiftformat()
    try Shell.swiftlint(quiet: false)
    try Shell.swiftbuild()
    try Shell.sourcekitten(module: "AppStoreConnectKit", destination: Shell.cwd + "/docs/source.json")
    try Shell.jazzy()
    try Shell.gitAdd(.all)
    try Shell.gitCommit(message: "added documentation")
  }
}
