 

Feature | Owner | Details
:-------------------- | :--------------------| :--------------------|
integration with [DarkMatter](https://gitlab.com/thecb4/DarkMatter) | [@thecb4](https://gitlab.com/thecb4) | Built in encode / decode
fix locales (in-app-purchase, achievement, leaderboard) | [@thecb4](https://gitlab.com/thecb4) | Need to create protocols
