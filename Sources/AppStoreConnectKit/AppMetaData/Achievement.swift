/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation
import XMLCoder

/**
 Container for array of Achievements.
 */
public struct Achievements: Codable {
  // let string: String
  public var achievements: [Achievement]
  enum CodingKeys: String, CodingKey {
    // case string
    case achievements = "achievement"
  }
}

/**
 Achievements are specific goals that the user can accomplish by reaching a
 milestone or performing an action within your game (for example, “find 10 gold
 coins” or “capture the flag in less than 30 seconds”). You can customize the
 text that describes an achievement, the number of points a player earns by
 completing an achievement, and the image that is displayed after an achievement
 has been earned. A player can see their earned achievements within the Game
 Center app; they can also compare their achievements with those earned by a
 friend.

 */
public struct Achievement: Codable {
  public var remove: Bool?
  public var position: Int?
  public var achievementID: String
  public var referenceName: String?
  public var points: Int?
  public var isHidden: Bool?
  public var isReusable: Bool?
  public var locales: Locales?

  enum CodingKeys: String, CodingKey {
    case remove
    case position
    case achievementID = "achievement_id"
    case referenceName = "reference_name"
    case points
    case isHidden = "hidden"
    case isReusable = "reusable"
    case locales
  }

  static func nodeEncoding(forKey key: CodingKey) -> XMLEncoder.NodeEncoding {
    switch key {
      case CodingKeys.position, CodingKeys.remove:
        return .attribute
      default:
        return .element
    }
  }
}
