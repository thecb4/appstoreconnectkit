/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 A representation of the
 [App Metadata](https://help.apple.com/itc/appsspec/en.lproj/static.html)
 xml format as specified by Apple.

 All metadata files must use UTF-8 Unicode character encoding. UTF-8 efficiently
 encodes non-Roman characters and helps to ensure that metadata displayed in the
 store is the same as was intended by the repertoire owner. Your metadata file
 should not contain a byte-order mark (BOM) as it is not necessary for UTF-8
 encoded data files nor is it supported by the App Store at this time.
 Incorrectly encoded metadata files or files that include a BOM will cause delays
 or may prevent your content from importing.

 This specification defines character lengths in bytes. Japanese language
 characters are represented in 3 bytes so be sure not to exceed the character
 lengths when including these characters.

 Note that simply including encoding="UTF-8" in your XML declaration is not
 sufficient. The file itself must also be correctly encoded for accented
 characters and punctuation to appear correctly in the App Store.

 */
public struct AppMetaData: Codable {
  /**
   This value should be the App Store-defined provider shortname given for
   partner identification. If you do not know your provider shortname, use the
   <team_id> tag to supply your team's Company/Organization ID.
   */
  public var provider: String

  /**
   This value identifies your organization's team in the Apple Developer Program
   and is 10 characters long, consisting of numbers and uppercase letters.
   You can find your Team ID in the Membership section of your account on the
   Apple Developer website.
   */
  public var teamID: String

  /// Begins the software element block. Only one software element can be defined per package.
  public var software: Software

  enum CodingKeys: String, CodingKey {
    case provider
    case teamID = "team_id"
    case software
  }
}
