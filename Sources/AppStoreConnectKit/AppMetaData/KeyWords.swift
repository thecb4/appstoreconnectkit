/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 One or more keywords that describe your app. Keywords are used to help customers
 find your app on the App Store effectively. You can supply single keywords or a
 phrase within a <keyword> tag, or you can supply multiple keywords separated by
 commas within a <keyword> tag (for example, <keyword>education, fun,
 words</keyword>). There is a limit of 100 characters for all keywords, including
 any commas. Customers can search your app by app name, company name, and keywords.

 Note: Make sure the keywords do not violate the rights or trademarks of any
 third parties. Keywords must be related to your app content and cannot contain
 offensive or trademarked terms, other app names, or company names. If you deliver
 a keyword that is trademarked or that references another app’s name or company
 name, your app may be removed from the App Store.
 */
public struct KeyWords: Codable {
  public var keywords: [String]

  enum CodingKeys: String, CodingKey {
    case keywords = "keyword"
  }
}
