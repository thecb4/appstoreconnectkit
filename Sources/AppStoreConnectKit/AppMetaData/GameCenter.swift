/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 Game Center is Apple’s social gaming network. This chapter provides information
 on delivering Game Center metadata for leaderboards and achievements, as well
 as providing localizations.

 Use App Store Connect to enable your app for Game Center testing, and deliver
 the XML to set up your leaderboards and achievements. Then use Game Kit
 Framework to integrate Game Center content into your app. When you are ready to
 distribute your app, enable your specific app version for Game Center and
 submit it to the App Store.

 Note: Leaderboards and achievements can be set up using App Store Connect or
 through the metadata XML feed. So, it is possible that your game has some
 existing leaderboards and/or achievements. When sending leaderboard and
 achievement metadata (including localizations) through the feed, you are
 indicating that what you are sending is the current state of that leaderboard
 or achievement. For example, if you are adding or modifying a leaderboard or
 achievement, include it and the localizations that you want to add or modify in
 the metadata. If there is an existing leaderboard or achievement and you want
 to keep it as is, you can omit the leaderboard or achievement from the metadata
 XML. Likewise, you can omit the localizations that are to be kept as is.

 */
public struct GameCenter: Codable {
  public var achievements: Achievements
  public var leaderboards: LeaderBoards
}
