/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 Only one software element can be defined per package.
 */
public struct Software: Codable {
  /**
   The permanent value that uniquely identifies the software app separately from
   any other software app given by the provider. This ID is used for tracking
   your assets and reporting. Vendor identifiers cannot be reused. The vendor_id
   is the app’s SKU in App Store Connect.

   Restrictions: The vendor identifier can only contain alphanumeric characters
   and underscore marks; it cannot contain spaces, dashes, ampersands, other
   punctuation, or symbols. The vendor identifier is case-sensitive and must not
   start with an underscore. If a Vendor ID is numeric, it is treated as a
   string, not numbers; a vendor identifier of '00000000012345' is not the same
   as '12345'. The Vendor ID must be at least 2 characters but no more than 100
   bytes.

   */
  public var vendorID: String

  /**
   Begins the metadata element block that includes tags for describing the app.

   (Optional) You can specify a platform using the app_platform attribute, for
   example:

   <software_metadata app_platform="appletvos">

   The attribute indicates that the metadata in this XML delivery applies to that
   platform. Allowed values for app_platform include: ios, osx, and appletvos.

   Note: If you send a metadata update for an existing iOS and you specify
   appletvos with the attribute, the App Store automatically creates a version for
   Apple TV.
   */
  public var metadata: MetaData

  enum CodingKeys: String, CodingKey {
    case vendorID = "vendor_id"
    case metadata = "software_metadata"
  }
}
