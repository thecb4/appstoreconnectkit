/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

public struct Locales: Codable {
  public var locales: [Locale]

  enum CodingKeys: String, CodingKey {
    // case string
    case locales = "locale"
  }
}

extension Locales {
  public subscript(_ name: String) -> Locale? {
    get {
      return locales.filter { $0.name == name }.first
    }
    set(newValue) {
      guard let value = newValue else {
        fatalError("Cannot pass nil Locale")
      }

      if let index = self.locales.firstIndex(where: { $0.name == value.name }) {
        locales[index] = value
      } else {
        locales += [value]
      }
    }
  }
}

public struct Locale: Codable {
  public var name: String
  public var remove: Bool?
  public var title: String?
  public var subtitle: String?
  public var description: String?
  public var promotionalText: String?
  public var keywords: KeyWords?
  public var whatsNew: String?
  public var softwareURL: URL?
  public var privacyURL: URL?
  public var supportURL: URL?
  public var appPreviews: AppPreviews?
  public var screenShots: ScreenShots?

  enum CodingKeys: String, CodingKey {
    case name
    case remove
    case title
    case subtitle
    case description
    case promotionalText = "promotiona_text"
    case keywords
    case whatsNew = "version_whats_new"
    case softwareURL = "software_url"
    case privacyURL = "privacy_url"
    case supportURL = "support_url"
    case appPreviews = "app_previews"
    case screenShots = "software_screenshots"
  }

  // https://theswiftpost.co/swift-4s-codable/
}
