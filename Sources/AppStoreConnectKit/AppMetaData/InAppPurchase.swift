/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 Container for in app purchases array
 */
public struct InAppPurchases: Codable {
  // let string: String
  public var inAppPurchases: [InAppPurchase]

  enum CodingKeys: String, CodingKey {
    // case string
    case inAppPurchases = "in_app_purchase"
  }
}

/**
 This chapter provides information on delivering in-app purchases to the App Store.
 In-app purchases allow you to sell additional features and functionality from
 within your free or paid iOS, tvOS, and macOS apps. To learn more, see In-App
 Purchase for Developers.

 Note: To offer in-app purchases in your apps, you will need to have the latest
 Paid Applications agreement in effect with Apple. Your Team Agent can request
 the latest Paid agreement in App Store Connect. Your Team Agent will also need
 to click through the latest Program License Agreement in the Membership section
 of your account on the Apple Developer website.
 */
public struct InAppPurchase: Codable {
  public var productID: String
  public var referenceName: String?
  public var type: String?
  public var products: Products?
  public var locales: Locales?

  enum CodingKeys: String, CodingKey {
    case productID = "product_id"
    case referenceName = "reference_name"
    case type
    case products
    case locales
  }
}
