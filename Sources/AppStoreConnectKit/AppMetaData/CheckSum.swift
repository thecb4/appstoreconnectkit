/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation
import XMLCoder

/**
 All content files (such as the review screenshots) delivered to App Store must
 include an industry-standard MD5 checksum, a measure that helps to guarantee
 that the files received by the App Store are correct and complete.
 Once received, the MD5 checksum contained in the metadata file is compared
 against the actual screenshot file received by the App Store. If any differences
 are detected, the screenshot file and the entire corresponding package are
 rejected.

 */
public struct CheckSum: Codable, DynamicNodeEncoding {
  public var type: String
  public var value: String

  enum CodingKeys: String, CodingKey {
    case type
    case value
  }

  public static func nodeEncoding(forKey key: CodingKey) -> XMLEncoder.NodeEncoding {
    switch key {
      case CodingKeys.type:
        return .attribute
      default:
        return .element
    }
  }
}
