/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation

/**
 Container for array of App Previews.
 */
public struct AppPreviews: Codable {
  public var appPreviews: [AppPreview]
  enum CodingKeys: String, CodingKey {
    case appPreviews = "app_preview"
  }
}

/**
 App preview requirements have been updated for the new generation of iPad.
 You can now upload app previews for the new 12.9-inch iPad Pro and 11-inch iPad Pro.
 New values have been added for use with the display_target attribute on an
 <app_preview> tag: iOS-iPad-Pro-2018 and iOS-iPad-Pro-11-in.

 If your app is the same across multiple device sizes and localizations, provide
 just the highest resolution app preview set for each device type. For iPad, app
 previews for the 12.9-inch iPad Pro (2nd generation) are required. App previews
 for the 12.9-inch iPad Pro (3rd generation) are optional and also display with
 rounded corners. These app previews will scale down for the 11-inch iPad Pro,
 but not for older iPad devices.
 */
public struct AppPreview: Codable {
  public var displayTarget: String
  public var position: Int
  public var previewImageTime: PreviewImageTime?
  public var dataFile: DataFile

  enum CodingKeys: String, CodingKey {
    case displayTarget = "display_target"
    case position
    case previewImageTime = "preview_image_time"
    case dataFile = "data_file"
  }
}
