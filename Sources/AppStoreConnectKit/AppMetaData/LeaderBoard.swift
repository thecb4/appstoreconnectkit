/**

 Created by Cavelle Benjamin on 19-Feb-07 (06).

 [MIT License](https://choosealicense.com/licenses/mit/#)

 Copyright (c) 2019 'Cavelle Benjamin'

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

import Foundation
import XMLCoder

/**
 Container for leader board array
 */
public struct LeaderBoards: Codable {
  // let string: String
  public var leaderboards: [LeaderBoard]
  enum CodingKeys: String, CodingKey {
    // case string
    case leaderboards = "leaderboard"
  }
}

/**
 Leaderboard (At least one <leaderboard> is required if <leaderboards> block is supplied)

 Identifies an individual leaderboard. You can define multiple <leaderboard>
 blocks as needed, up to 25 for each app. For example, you might have a
 leaderboard for each game level: Easy, Medium, and Hard.

 Use the default attribute when you have multiple leaderboards and you want to
 indicate that this leaderboard is the default leaderboard (the first leaderboard
 that the user sees on the device). Each app can have only one default leaderboard.
 Note that you can move the default from one leaderboard to another by including
 both leaderboards in the package, omitting the default="true" from the current
 default leaderboard (or setting it to "false") and specifying default="true" for
 the new default leaderboard.

 The position attribute determines the order in which the leaderboards are
 presented to users. You can change the order on the app’s Game Center page or by
 sending a metadata update.

 Note that once a leaderboard is available to the user for any version of your app,
 it cannot be deleted.
 */
public struct LeaderBoard: Codable {
  public var isDefault: Bool?
  public var position: Int?
  public var remove: Bool?
  public var leaderBoardID: String?
  public var referenceName: String?
  public var aggregateParentLeaderboard: String?
  public var sortAscending: Bool?
  public var scoreRangeMin: Int?
  public var scoreRangeMax: Int?
  public var locales: Locales?

  enum CodingKeys: String, CodingKey {
    case isDefault = "default"
    case position
    case leaderBoardID = "leaderboard_id"
    case referenceName = "reference_name"
    case aggregateParentLeaderboard = "aggregate_parent_leaderboard"
    case sortAscending = "sort_ascending"
    case scoreRangeMin = "score_range_min"
    case scoreRangeMax = "score_range_max"
    case locales
  }

  static func nodeEncoding(forKey key: CodingKey) -> XMLEncoder.NodeEncoding {
    switch key {
      case CodingKeys.isDefault, CodingKeys.position:
        return .attribute
      default:
        return .element
    }
  }
}
