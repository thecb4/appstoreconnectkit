# AppStoreConnect Change Log
All notable changes to this project will be documented in this file.

* Format based on [Keep A Change Log](https://keepachangelog.com/en/1.0.0/)
* This project adheres to [Semantic Versioning](http://semver.org/). 

## [0.1.0] - 2019-Feb-23. 
### Added
- Added app metadata based on https://help.apple.com/itc/appsspec/en.lproj/static.html
- Added markdown documentation
- gitlab-ci

### Changed
-

### Deprecated
-

### Removed
-

### Fixed
-

### Security
-

(4)