import XCTest

import AppStoreConnectTests

var tests = [XCTestCaseEntry]()
tests += AppStoreConnectTests.allTests()
XCTMain(tests)
