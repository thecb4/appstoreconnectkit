import XCTest
import AppStoreConnectKit
import XMLCoder

final class AppStoreConnectKitTests: XCTestCase {
  func testDecodeAppMetadata() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct
    // results.

    do {
      let inputPath = FileManager.default.currentDirectoryPath + "/Tests/Fixtures/metadata.xml"
      let inputURL = URL(fileURLWithPath: inputPath)
      let input = try String(contentsOf: inputURL)
      let decoder = XMLDecoder()
      let metadata = try decoder.decode(AppMetaData.self, from: input.data(using: .utf8)!)
      print(metadata.software.metadata.gameCenter)
    } catch {
      print(error)
      XCTFail()
    }
  }

  static var allTests = [
    ("testDecodeAppMetadata", testDecodeAppMetadata)
  ]
}
